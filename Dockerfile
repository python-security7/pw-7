FROM python:3.9.6

WORKDIR /backend

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN pip install --upgrade pip
COPY requirements.txt /backend
RUN pip install -r requirements.txt

COPY . /backend/

EXPOSE 5050
