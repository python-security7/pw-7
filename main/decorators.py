def retry(attempts):
    def retry_decorator(func):
        def _wrapper(*args, **kwargs):
            for _ in range(attempts):
                try:
                    return func(*args, **kwargs)
                except:
                    print('catch error')
            return "attempts ended!"
        return _wrapper
    return retry_decorator
