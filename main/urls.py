from django.urls import path
from .views import *

urlpatterns = [
    path('', Base.as_view(), name='base_urls'),
    path('pharmacy/advt/<str:advt>', AdvtInfo.as_view(), name='advt_info_urls')
]
