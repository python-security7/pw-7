from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
import datetime
import logging
from main.utils import get_parsed_data

logger = logging.getLogger(__name__)


class Base(APIView):
    def get(self, request):
        logger.warning('Homepage accessed at ' + str(datetime.datetime.now()) + ' hours!')
        return Response({"message": "Hello World"}, status=status.HTTP_200_OK)


class AdvtInfo(APIView):
    def get(self, request, advt: str):
        is_fine, result = get_parsed_data(advt)
        if is_fine:
            return Response(result, status=status.HTTP_200_OK)
        return Response(result, status=status.HTTP_404_NOT_FOUND)
