import datetime
import lxml.html
import requests
import logging

from main.decorators import retry
logger = logging.getLogger(__name__)


def parse_lots(advt):
    lot_html = requests.get(url=f"https://fms.ecc.kz/ru/announce/index/{advt}?tab=lots", verify=False)
    val = lxml.html.fromstring(lot_html.text)
    data_lots = val.xpath('//table[@class="table table-bordered table-hover table-striped"]/tr/td')

    sub_list = []
    total_lot = []
    count = 0

    for i in data_lots:
        if i.text == None:
            for j in i:
                sub_list.append(j.text.strip())
            continue
        sub_list.append(i.text)

        count += 1
        if count == 7:
            total_lot.append(sub_list)
            sub_list = []
            count = 0

    return total_lot


@retry(attempts=4)
def get_parsed_data(advt: str):
    all_lots = {}
    lots = []
    url = f'https://fms.ecc.kz/ru/announce/index/{advt}'
    res = {}
    html = requests.get(url=url, verify=False)
    logger.warning(f'Send request to https://fms.ecc.kz/ru/announce/index/{advt}' + str(datetime.datetime.now()) + ' hours!')
    if html.status_code == 404 or not html.text:
        logger.warning(
            'Data not retrieved' + str(datetime.datetime.now()) + ' hours!')
        return False, {"message": f"{advt} Not found!"}
    logger.warning(
        f'Data retrieved from https://fms.ecc.kz/ru/announce/index/{advt}' + str(datetime.datetime.now()) + ' hours!')
    data = lxml.html.fromstring(html.text)
    ad_number = data.xpath('//div[@class="panel-body"]/div/div[1]/div[1]/div[@class="col-sm-7"]/input/@value')
    ad_name = data.xpath('//div[@class="panel-body"]/div/div[1]/div[2]/div[@class="col-sm-7"]/input/@value')
    start_date = data.xpath('//div[@class="panel-body"]/div/div[2]/div[1]/div[@class="col-sm-7"]/input/@value')
    end_date = data.xpath('//div[@class="panel-body"]/div/div[2]/div[2]/div[@class="col-sm-7"]/input/@value')
    status_name = data.xpath('//div[@class="panel-body"]/div/div[1]/div[3]/div[@class="col-sm-7"]/input/@value')
    beneficiary = data.xpath('//table[@class="table table-bordered table-hover table-striped"][1]/tr[3]/td')
    beneficiary_bin = beneficiary[0].text.split(" ", 1)[0]
    beneficiary_name = beneficiary[0].text.split(" ", 1)[1]

    total_lot = parse_lots(advt)

    for row in total_lot:
        all_lots['adNumber'] = ad_number[0]
        all_lots['adName'] = row[1]
        all_lots['lotName'] = row[3]
        all_lots['amount'] = row[6]
        all_lots['statusName'] = row[7]
        lots.append(all_lots)
        all_lots = {}
    res['adNumber'] = ad_number[0]
    res['adName'] = ad_name[0]
    res['startDate'] = start_date[0]
    res['endDate'] = end_date[0]
    res['statusName'] = status_name[0]
    res['lots'] = lots
    res['beneficiaryBin'] = beneficiary_bin
    res['beneficiaryName'] = beneficiary_name
    return True, res
